import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum AppTheme {
  dark,
  light,
}

final appThemes = {
  AppTheme.dark: ThemeData(
    primaryColor: Colors.cyan,
    accentColor: Colors.cyan,
    brightness: Brightness.dark,
    fontFamily: GoogleFonts.aBeeZee().fontFamily,
  ),
  AppTheme.light: ThemeData(
    primaryColor: Colors.cyan,
    splashColor: Color(0xDDFDEDF3),
    accentColor: Colors.cyan,
    brightness: Brightness.light,
    fontFamily: GoogleFonts.aBeeZee().fontFamily,
  ),
};
